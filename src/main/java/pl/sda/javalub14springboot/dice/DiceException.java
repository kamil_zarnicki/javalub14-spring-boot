package pl.sda.javalub14springboot.dice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DiceException extends RuntimeException {

    public DiceException() {
        super("Sides param must be between 1 and 100.");
    }
}
