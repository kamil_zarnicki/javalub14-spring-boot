package pl.sda.javalub14springboot.foobar;

import org.springframework.stereotype.Service;

@Service
public class BarService {

    private final FooBarProperties fooBarProperties;

    public BarService(FooBarProperties fooBarProperties) {
        this.fooBarProperties = fooBarProperties;
    }

    public String bar() {
        return fooBarProperties.getBar();
    }
}
