package pl.sda.javalub14springboot.employee;

import java.util.StringJoiner;

public class EmployeeDTO extends EmployeeMinDTO {

    private Integer salary;
    private String department;

    public EmployeeDTO(Long id, String name, Integer salary, String department) {
        super(id, name);
        this.salary = salary;
        this.department = department;
    }

    public EmployeeDTO() {
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EmployeeDTO.class.getSimpleName() + "[", "]")
                .add("salary=" + salary)
                .add("department='" + department + "'")
                .toString();
    }
}
