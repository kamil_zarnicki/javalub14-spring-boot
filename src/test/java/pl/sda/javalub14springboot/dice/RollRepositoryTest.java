package pl.sda.javalub14springboot.dice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RollRepositoryTest {

    @Autowired
    private RollRepository rollRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    public void findAllBySides() {
        //Given
        testEntityManager.persist(new RollEntity(6, 5));
        testEntityManager.persist(new RollEntity(10, 9));
        testEntityManager.flush();

        //When
        List<RollEntity> result = rollRepository.findAllBySides(6, Sort.by("date"));

        //Then
        RollEntity rollResult = retrieveOrFail(result);
        assertThat(result.size(), is(1));
        assertThat(rollResult.getSides(), is(6));
        assertThat(rollResult.getResult(), is(5));
    }

    private RollEntity retrieveOrFail(List<RollEntity> result) {
        return result.stream()
                .findFirst()
                .orElseGet(() -> {
                    fail();
                    return null;
        });
    }
}