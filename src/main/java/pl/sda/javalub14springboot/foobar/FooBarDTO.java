package pl.sda.javalub14springboot.foobar;

public class FooBarDTO {

    private Long id;
    private String foo;
    private String bar;

    public FooBarDTO() {
    }

    public FooBarDTO(Long id, String foo, String bar) {
        this.id = id;
        this.foo = foo;
        this.bar = bar;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFoo() {
        return foo;
    }

    public void setFoo(String foo) {
        this.foo = foo;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("FooBarDTO{");
        sb.append("id=").append(id);
        sb.append(", foo='").append(foo).append('\'');
        sb.append(", bar='").append(bar).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
