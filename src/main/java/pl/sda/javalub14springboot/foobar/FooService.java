package pl.sda.javalub14springboot.foobar;

import org.springframework.stereotype.Service;

@Service
public class FooService {

    private final BarService barService;
    private final FooBarProperties fooBarProperties;

    public FooService(BarService barService, FooBarProperties fooBarProperties) {
        this.barService = barService;
        this.fooBarProperties = fooBarProperties;
    }

    public String foo() {
        return fooBarProperties.getFoo();
    }

    public String fooBar() {
        return foo() + barService.bar();
    }
}
