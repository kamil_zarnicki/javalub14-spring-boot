package pl.sda.javalub14springboot.thymleaf;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {


    @GetMapping("/thymleaf")
    public String hello(Model model) {
        model.addAttribute("name", "JavaLub14");
        return "hello";
    }
}
