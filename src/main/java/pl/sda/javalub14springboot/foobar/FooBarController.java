package pl.sda.javalub14springboot.foobar;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/foobar")
public class FooBarController {

    private final Logger log = LoggerFactory.getLogger(FooBarController.class);
    private final AtomicLong atomicLong = new AtomicLong(0);

    @GetMapping
    public String foo() {
        return "Foo!";
    }

    @GetMapping("/foo")
    public FooBarDTO fooBar(@PathParam("foo") String foo, @PathParam("bar") String bar) {
        return new FooBarDTO(12l, foo, bar);
    }

//    @GetMapping("/foo/{foo}/{bar}")
//    public FooBarDTO fooBar(@PathVariable("foo") String foo, @PathVariable("bar") String bar) {
//        return new FooBarDTO(12l, foo, bar);
//    }

    @PostMapping
    public ResponseEntity<Void> postFoo(@RequestBody FooBarDTO dto) {
        log.info("postFoo() -> {}", dto);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PostMapping("/validate")
    public ResponseEntity<Void> validate(@RequestBody FooBarDTO dto) {
        log.info("validate() -> {}", dto);
        if (dto.getId() != null) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        } else {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
        }
    }

    @PostMapping("/setid")
    public ResponseEntity<FooBarDTO> setId(@RequestBody FooBarDTO dto) {
        log.info("setId() -> {}", dto);
        if (dto.getId() == null) {
            dto.setId(atomicLong.incrementAndGet());
        } else {
            throw new FooException();
        }
        return ResponseEntity.ok(dto);
    }

}
