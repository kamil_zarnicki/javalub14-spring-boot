package pl.sda.javalub14springboot.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final Logger log = LoggerFactory.getLogger(EmployeeService.class);

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public void createOrUpdate(EmployeeDTO dto) {
        log.info("createOrUpdate with: {}", dto);

        Employee e = new Employee(dto.getId(),
                dto.getName(),
                dto.getSalary(),
                dto.getDepartment());

        employeeRepository.save(e);
        log.info("Employee with id {} saved.", e.getId());
    }

    public List<EmployeeMinDTO> getEmployees() {
        log.info("getEmployees");
        return employeeRepository.findAll().stream()
                .map(e -> new EmployeeMinDTO(e.getId(), e.getName()))
                .collect(Collectors.toList());
    }

    public EmployeeDTO getOne(Long id) {
        log.info("getOne id: {}", id);
        Employee e = employeeRepository.getOne(id);
        return new EmployeeDTO(e.getId(),
                e.getName(),
                e.getSalary(),
                e.getDepartment());
    }

    public void remove(Long id) {
        log.info("remove Employee with id {}", id);
        employeeRepository.deleteById(id);
    }
}
