package pl.sda.javalub14springboot.employee;

public class EmployeeMinDTO {

    private Long id;
    private String name;

    public EmployeeMinDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public EmployeeMinDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
