package pl.sda.javalub14springboot.dice;

import java.time.LocalDateTime;

public class RollDTO extends RollMinDTO {

    private Integer sides;
    private LocalDateTime date;

    public RollDTO(Long id, Integer result, Integer sides, LocalDateTime date) {
        super(id, result);
        this.sides = sides;
        this.date = date;
    }

    public Integer getSides() {
        return sides;
    }

    public void setSides(Integer sides) {
        this.sides = sides;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
