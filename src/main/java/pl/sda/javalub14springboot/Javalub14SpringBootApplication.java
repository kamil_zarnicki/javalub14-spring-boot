package pl.sda.javalub14springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import pl.sda.javalub14springboot.foobar.FooBarProperties;
import pl.sda.javalub14springboot.jpa.SampleRepository;

@EnableConfigurationProperties(FooBarProperties.class)
@SpringBootApplication
public class Javalub14SpringBootApplication {

	@Autowired
	private SampleRepository sampleRepository;

	public static void main(String[] args) {
		SpringApplication.run(Javalub14SpringBootApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		sampleRepository.save(new SampleEntity("nazwa", 10, true));
//		sampleRepository.save(new SampleEntity("nowy", 12, true));
//		sampleRepository.save(new SampleEntity("javalub14", 8, true));
//		sampleRepository.save(new SampleEntity("javawaw10", 20, false));

//		System.out.println("Count: " + sampleRepository.count());
//		List<SampleEntity> result = sampleRepository.findAll();
//		List<SampleEntity> result = sampleRepository.findAllActive();

//		result.forEach(sampleEntity -> System.out.println(sampleEntity));

//	}
}
