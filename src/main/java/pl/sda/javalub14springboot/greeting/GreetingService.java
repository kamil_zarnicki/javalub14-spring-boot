package pl.sda.javalub14springboot.greeting;

import org.springframework.stereotype.Service;

@Service
public class GreetingService {

    private final NameService nameService;

    public GreetingService(NameService nameService) {
        this.nameService = nameService;
    }

    public String sayHello() {
        return sayHello(nameService.getName());
    }

    public String sayHello(String name) {
        return String.format("Hello %s!", name);
    }
}
