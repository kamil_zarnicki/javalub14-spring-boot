package pl.sda.javalub14springboot.dice;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RollRepository extends JpaRepository<RollEntity, Long> {

    List<RollEntity> findAllBySides(int sides, Sort sort);
}
