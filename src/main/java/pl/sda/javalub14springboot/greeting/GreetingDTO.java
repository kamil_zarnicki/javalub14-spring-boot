package pl.sda.javalub14springboot.greeting;

public class GreetingDTO {

    private Long id;
    private String greeting;

    public GreetingDTO() {
    }

    public GreetingDTO(Long id, String greeting) {
        this.id = id;
        this.greeting = greeting;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GreetingDTO{");
        sb.append("id=").append(id);
        sb.append(", greeting='").append(greeting).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
