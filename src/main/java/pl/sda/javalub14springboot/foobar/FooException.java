package pl.sda.javalub14springboot.foobar;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
public class FooException extends RuntimeException {

    public FooException() {
        super("Object already has ID.");
    }
}
