package pl.sda.javalub14springboot.greeting;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class GreetingControllerTest {

    private static final String HELLO_JAVA = "Hello Java!";
    private static final String HELLO_KAMIL = "Hello Kamil!";
    public static final String CONTENT_WITHOUT_ID = "{\"greeting\": \"Hello\"}";

    private MockMvc mockMvc;

    @Mock
    private GreetingService greetingService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(greetingService.sayHello()).thenReturn(HELLO_JAVA);
        Mockito.when(greetingService.sayHello("Kamil")).thenReturn(HELLO_KAMIL);
        this.mockMvc = MockMvcBuilders.standaloneSetup(new GreetingController(greetingService))
                .build();
    }

    @Test
    public void testHello() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/greeting"))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.greeting").value(HELLO_JAVA));
    }

    @Test
    public void testHelloWithNameParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/greeting/hello")
                    .param("name", "Kamil"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.greeting").value(HELLO_KAMIL));
    }

    @Test
    public void setIdShouldSetIdWhenIdIsNull() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/greeting/setid")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(CONTENT_WITHOUT_ID))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.greeting").value("Hello"));
    }
}