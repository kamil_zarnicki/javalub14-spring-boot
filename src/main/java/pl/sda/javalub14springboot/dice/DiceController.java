package pl.sda.javalub14springboot.dice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dice")
public class DiceController {

    private final DiceService diceService;

    public DiceController(DiceService diceService) {
        this.diceService = diceService;
    }

    @GetMapping("/roll")
    public RollMinDTO roll(@RequestParam(name = "sides", required = false, defaultValue = "6")
                                    Integer sides) {
        return diceService.roll(sides);
    }

    @GetMapping("/history")
    public List<RollDTO> getHistoricRolls() {
        return diceService.getHistoricData();
    }

    @GetMapping("/history/{sides}")
    public List<RollDTO> getHistoricRolls(@PathVariable("sides") Integer sides) {
        return diceService.getHistoricData(sides);
    }
}
