package pl.sda.javalub14springboot.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final Logger log = LoggerFactory.getLogger(EmployeeController.class);

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeMinDTO> listEmployees() {
        log.info("GET /employee");
        return employeeService.getEmployees();
    }

    @GetMapping("/{id}")
    public EmployeeDTO getOne(@PathVariable("id") Long id) {
        log.info("GET /employee for id {}", id);
        return employeeService.getOne(id);
    }

    @PostMapping
    public void createOrUpdate(@RequestBody EmployeeDTO dto) {
        log.info("POST /employee with {} ", dto);
        employeeService.createOrUpdate(dto);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable("id") Long id) {
        log.info("DELETE /employee/{}", id);
        employeeService.remove(id);
    }
}
