package pl.sda.javalub14springboot.greeting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

    private final GreetingService greetingService;
    private final AtomicLong id = new AtomicLong(0);
    private final Logger log = LoggerFactory.getLogger(GreetingController.class);

    public GreetingController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    @GetMapping
    public GreetingDTO hello() {
        GreetingDTO dto = new GreetingDTO();
        dto.setId(id.incrementAndGet());
        dto.setGreeting(greetingService.sayHello());
        return dto;
    }

    @GetMapping("/hello")
    public GreetingDTO hello(@PathParam("name") String name) {
        return new GreetingDTO(id.incrementAndGet(), greetingService.sayHello(name));
    }

    @PostMapping("/setid")
    public ResponseEntity<GreetingDTO> setId(@RequestBody GreetingDTO dto) {
        log.info("setId -> {}", dto);
        if (dto.getId() == null) {
            dto.setId(id.incrementAndGet());
        } else if (dto.getId() <= 0) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
        }
        return ResponseEntity.ok(dto);
    }
}
