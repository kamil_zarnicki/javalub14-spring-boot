package pl.sda.javalub14springboot.dice;

public class RollMinDTO {
    private Long id;
    private Integer result;

    public RollMinDTO() {
    }

    public RollMinDTO(Long id, Integer result) {
        this.id = id;
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
