package pl.sda.javalub14springboot.dice;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class DiceService {

    private final Random random;
    private final RollRepository rollRepository;

    public DiceService(Random random, RollRepository rollRepository) {
        this.random = random;
        this.rollRepository = rollRepository;
    }

    public RollMinDTO roll(int sides) {
        if (sides <= 1 || sides > 100) {
            throw new DiceException();
        }
        int result = random.nextInt(sides) + 1;
        RollEntity entity = rollRepository.save(new RollEntity(sides, result));
        return new RollMinDTO(entity.getId(), entity.getResult());
    }

    public List<RollDTO> getHistoricData() {
        return rollRepository.findAll(Sort.by("date")).stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<RollDTO> getHistoricData(Integer sides) {
        return rollRepository.findAllBySides(sides, Sort.by("date")).stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private RollDTO toDto(RollEntity e) {
        return new RollDTO(e.getId(),
                e.getResult(),
                e.getSides(),
                e.getDate());
    }
}
