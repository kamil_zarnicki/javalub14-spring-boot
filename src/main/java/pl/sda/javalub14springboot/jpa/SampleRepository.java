package pl.sda.javalub14springboot.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SampleRepository extends JpaRepository<SampleEntity, Long> {

    List<SampleEntity> findAllByName(String name);

    List<SampleEntity> findAllByValueLessThan(Integer value);

    @Query("select s from SampleEntity s where s.active = true")
    List<SampleEntity> findAllActive();

    List<SampleEntity> findAllByActiveIsTrueAndValueIsLessThanEqual(Integer value);
}
