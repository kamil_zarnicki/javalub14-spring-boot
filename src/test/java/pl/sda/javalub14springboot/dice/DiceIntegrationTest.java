package pl.sda.javalub14springboot.dice;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Random;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestEntityManager
@Transactional
public class DiceIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private TestEntityManager testEntityManager;

    @MockBean
    private Random random;

    @Before
    public void setUp() throws Exception {
        when(random.nextInt(anyInt())).thenReturn(4);
    }

    @Test
    public void roll() {
        RollDTO result = testRestTemplate.getForObject("/dice/roll", RollDTO.class);

        assertThat(result.getResult(), is(5));
        RollEntity entity = testEntityManager.find(RollEntity.class, result.getId());
        assertThat(entity.getSides(), is(6));
        assertThat(entity.getResult(), is(5));
    }
}