package pl.sda.javalub14springboot.dice;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.sda.javalub14springboot.jpa.SampleRepository;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DiceController.class)
public class DiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DiceService diceService;

    @MockBean
    private SampleRepository sampleRepository;

    @Before
    public void setUp() {
        when(diceService.roll(anyInt())).thenReturn(new RollDTO(1l, 6, 6, null));
    }

    @Test
    public void rollReturnsResultFromService() throws Exception {
        mockMvc.perform(get("/dice/roll"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(6));
    }

    @Test
    public void rollExecutesServiceWithDefaultParam() throws Exception {
        mockMvc.perform(get("/dice/roll"))
                .andDo(print())
                .andExpect(status().isOk());

        verify(diceService).roll(eq(6));
    }

    @Test
    public void rollReturnsBadRequestWhenSidesIsInvalid() throws Exception {
        when(diceService.roll(anyInt())).thenThrow(new DiceException());

        mockMvc.perform(get("/dice/roll"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}